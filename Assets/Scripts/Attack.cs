﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class Attack : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    private WaitForSeconds _cooldown = new WaitForSeconds(1f);
    private bool _isCooldown;
    private Health _health;
    private bool _isEnemy = false;


    private void Strike()
    {
        _animator.SetTrigger("Strike");
        StartCoroutine(StrikeCooldown());
    }

    void Update()
    {
        if (Input.GetMouseButton(0) && !_isCooldown)
        {
            Strike();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.TryGetComponent<Health>(out Health opponentHealth))
        {
            _isEnemy = true;
            _health = opponentHealth;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (Input.GetMouseButton(0) && !_isCooldown && _isEnemy)
        {
            if (_health != null)
            {
                _health.TakeHit(20);
            }
            Strike();
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        _health = null;
        _isEnemy = false;
    }


    private IEnumerator StrikeCooldown()
    {
        _isCooldown = true;
        yield return _cooldown;
        _isCooldown = false;
        yield break;
    }
}
