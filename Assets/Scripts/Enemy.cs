﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private Transform _leftBorder;
    [SerializeField] private Transform _rightBorder;
    [SerializeField] private float _speed;
    [SerializeField] private SpriteRenderer _sprite;
    [SerializeField] private Animator _animator;

    private Health _health;
    private Vector3 _direction = Vector3.left;

    public void Strike()
    {
        if (_health != null)
        {
            _health.TakeHit(20);
        }        
    }

    void Update()
    {
        if (transform.position.x < _leftBorder.position.x)
        {
            _direction = Vector3.right;
            _sprite.flipX = true;
        }

        if (transform.position.x > _rightBorder.position.x)
        {
            _direction = Vector3.left;
            _sprite.flipX = false;
        }

        transform.Translate(_direction * _speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        _speed = 0;
        _animator.SetBool("Move", false);
        if (collision.gameObject.TryGetComponent<Health>(out Health opponentHealth))
        {
            _health = opponentHealth;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        _animator.SetBool("Strike", true);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        _animator.SetBool("Move", true);
        _animator.SetBool("Strike", false);
        _speed = 4;
        _health = null;
    }
}
