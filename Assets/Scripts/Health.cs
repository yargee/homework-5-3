﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] int _hitPoints;    
    public int HitPoints
    {
        get 
        { 
            return _hitPoints;
        }
        set
        {
            if (_hitPoints < 0)
            {
                _hitPoints = 0;
            }
            else _hitPoints = value;
        }
    }        
    
    public void TakeHit(int damage)
    {        
        HitPoints -= damage;

        if (HitPoints == 0)
        {
            Destroy(gameObject);
        }
    }
}
