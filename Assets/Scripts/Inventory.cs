﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    private int _jewelsNumber=0;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Jewel"))
        {
            _jewelsNumber++;
            Debug.Log($"Вы собрали {_jewelsNumber} камней");            
        }    
    }    
}
