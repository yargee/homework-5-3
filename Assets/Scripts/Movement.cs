﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private SpriteRenderer _sprite;
    [SerializeField] private float _speed;
    [SerializeField] private float _force;
    [SerializeField] private Animator _animator;
    [SerializeField] private Transform _lowerBorder;

    private bool _rightDirection = true;
    private bool _isGrounded;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Ground"))
        {
            _isGrounded = true;
        }        
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            _isGrounded = false;
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            if(_rightDirection)
            {
                _sprite.flipX = false;
                _rightDirection = !_rightDirection;
            }            
            _rigidbody.transform.Translate(Vector3.left * _speed * Time.deltaTime);
            _animator.SetFloat("runningSpeed", 1);           
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            _animator.SetFloat("runningSpeed", 0);
        }


        if (Input.GetKey(KeyCode.D))
        {
            if (!_rightDirection)
            {
                _sprite.flipX = true;
                _rightDirection = !_rightDirection;
            }
            
            _rigidbody.transform.Translate(Vector3.right * _speed * Time.deltaTime);
            _animator.SetFloat("runningSpeed", 1);            
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            _animator.SetFloat("runningSpeed", 0);
        }


        if (Input.GetKeyDown(KeyCode.Space) && _isGrounded)
        {
            _rigidbody.AddForce(Vector3.up * _force, ForceMode2D.Impulse);                           
        }

        if (_isGrounded)
        {
            _animator.SetBool("isGrounded", true);
        }
        else
        {
            _animator.SetBool("isGrounded", false);
        }

        if(transform.position.y < _lowerBorder.transform.position.y)
        {
            Destroy(gameObject);
        }
    }
}
